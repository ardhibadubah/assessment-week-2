const squareEveryDigit = (num) => {
    num = num.toString()
    let temp = ''
    for (let i = 0; i < num.length; i++) {
    temp += parseInt(num[i]) ** 2
    }
    return temp
};

const input = "9119";

console.log(squareEveryDigit(input));
