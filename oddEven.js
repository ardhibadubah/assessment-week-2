const oddEven = (input) => {
    let result
    if(input % 2 === 1) {
        result = `${input} is odd number`
    } else if(input % 2 === 0){
        result = `${input} is even number`
    } else {
        result = 'your input value is not valid'
    }
    return result
};

const input = 5;

console.log(oddEven(input));