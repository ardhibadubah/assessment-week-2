const fizzBuzz = (input) => {
    let value = ""
    for (let i = 1; i <= input; i++){
        if(i % 3 === 0 && i % 5 === 0){
            value += 'FizzBuzz '
        }else if(i % 3 === 0){
            value += 'Fizz '
        }else if(i % 5 === 0){
            value += 'Buzz '
        }else{
            value += i + ' '
        }
    }    

    return value
};

const input = 16;

console.log(fizzBuzz(input));
