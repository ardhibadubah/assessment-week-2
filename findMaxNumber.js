const findMaxNumber = (input) => {
    let max = input[0];
    for(let i = 0; i < input.length; i++){
        if(max < input[i]){
            max = input[i]
        }
    }

    return max
};

const input = [8, 2, 5, 10, 18, 9, 15];

console.log(findMaxNumber(input));
